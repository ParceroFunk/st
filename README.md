# ParceroFunk build for the _st_ terminal

## Patches to date
- [alpha](https://st.suckless.org/patches/alpha/): to give the terminal a transparent background.
- [scrollback](https://st.suckless.org/patches/scrollback/): to give the terminal scrollback functionality.
- [font2](https://st.suckless.org/patches/font2/): extra font for fallback situation.
